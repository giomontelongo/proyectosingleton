package Cartas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Deck {
	
	private ArrayList<Card> cards;
	private volatile static Deck uniqueInstance;
	
	public Deck() {
	    cards = new ArrayList<Card>( );

	    // build the deck
	    Suit[] suits = {Suit.SPADES, Suit.HEARTS, Suit.CLUBS, Suit.DIAMONDS};
	    for(Suit suit: suits) {
	      for(int i = 2; i <= 14; i++) {
	        cards.add(new Card(suit, i));
	      }
	    }

	    // shuffle it!
	    Collections.shuffle(cards, new Random( ));
	  }

	public void print( ) {
	    for(Card card: cards) {
	      card.print( );
	    }
	}
	
	public static Deck getInstance(){
		if (uniqueInstance == null) {
			synchronized (Deck.class) {
				if(uniqueInstance == null){
					System.out.println("Creating unique instance of Deck");
					uniqueInstance = new Deck();
				}
			}
		}
		System.out.println("Returning instance of Deck");
		return uniqueInstance;
	}
}
